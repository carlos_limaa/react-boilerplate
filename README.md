# Prerequisites
Nodejs version: `^8.5.0`

NPM version: ``5.3.0`

# Installation
From terminal use commands npm or yarn(prefer):
`npm install`
Stylefmt use for CSS format files.
`npm i stylefmt -g`

# Configuration
Folder *conf/* have folders development, production and test.
They have a index.json file configuration.
* Here can modify ~/endpoints.
* Validations.
* Error codes.
* Some basic components modifications.

## Conf.staticFilesServer
Change this value is boolean, this configuration show or remove *#/* after index.html file.
If your domain hide file `index.html` put this value to false.

# Development
Open terminal and type `npm run dev`, this command up Chrome browser with localhost:xxxx for start to development mode.
All files you need to update are on folder *conf/development* and *src/*.
On development mode you can use Chrome [Redux DevTools Extension](http://extension.remotedev.io/ "Redux DevTools").

On second terminal run `npm run test`, if you want test in real time when are you coding. This is testing framework called "Jest" on `package.json` is configuration.

## Testing
Open terminal and type `npm run test:coverage` for run all test with.
* Connection api.
* Renders.

# Build, compile output SPA application.
Open terminal and type `npm run build`, all files are out on folder *dist/*.

# Format standar code
Capability for making a very good standard code. For css files and javascript, you can edit rules on `.stylelintrc` for css files. And `.eslintrc.json` for same validation and formatting code.

Command for css run: `npm run format:css`.
Command for javascript files run: `npm run format:js` (now not compatible on Windows).

# Scallfolds (Only for Unix)
Run for terminal `./maker.sh` and follow menu instructions.

# Routes
Are allowed `src/routers.js`.

# Compatibility javascript when code is build.
Tested browser compatibility with http://www.browserstack.com with next minimum requirements:
- Windows 7, IE10.
- Snow Leopard, Safari 5.1.
- IOS 5.1, iPhone 4S and Ipad 2.
- Android 4.4.

