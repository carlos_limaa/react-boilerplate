import Path from './path.js';
import webpack from 'webpack';
import merge from 'webpack-merge';
import baseConfig from './webpack.config.base';

module.exports = merge(baseConfig, {
    devtool: 'eval-source-map',
    context: Path.src, // `__dirname` is root of project and `src` is source
    entry: {
        app: './app.js',
    },
    output: {
        path: Path.dist, // `dist` is the destination
        filename: 'bundle.js',
        publicPath: '/assets/',
    },
    devServer: {
        open: true, // to open the local server in browser
        inline: true,
        contentBase: Path.src,
        historyApiFallback: true,
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/, // both .js and .jsx
                enforce: 'pre',
                loader: 'eslint-loader',
                include: Path.src,
                exclude: Path.node_modules,
                options: {
                    emitWarning: true, // enable compile with warnings.
                },
            },
            {
                test: /\.global\.css$/,
                use: [
                    { loader: 'style-loader' }, {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    }
                ]
            }, {
                test: /^((?!\.global).)*\.css$/,
                use: [
                    { loader: 'style-loader' }, {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            sourceMap: true,
                            importLoaders: 1,
                            localIdentName: '[name]__[local]__[hash:base64:5]',
                        }
                    },
                ]
            }, {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        mimetype: 'application/font-woff',
                    }
                },
            }, {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        mimetype: 'application/font-woff',
                    }
                }
            }, {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        mimetype: 'application/octet-stream'
                    }
                }
            }, {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: 'file-loader',
            }, {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        mimetype: 'image/svg+xml',
                    }
                }
            }, {
                test: /\.(?:ico|gif|png|jpg|jpeg|webp)$/,
                use: 'url-loader',
            }
        ]
    },
});
