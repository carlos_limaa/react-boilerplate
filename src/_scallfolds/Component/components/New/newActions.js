import Conf from 'conf';
import { __new__ } from 'api';

const api = new __new__(Conf.baseUrl);

export function get__new__(obj) {
    api.fromNameApiFunction(obj, (response) => {
        return (dispatch) => {
            dispatch({ type: 'HELPER_LOADING_ACTIVE' });
            const payload = {};
            dispatch({ type: 'HELPER_LOADING_INACTIVE' });
            return dispatch({ type: '_new_', payload });
        };
    });
}

export function loading(value) {
    if (value) {
        return { type: 'HELPER_LOADING_ACTIVE' };
    }
    return { type: 'HELPER_LOADING_INACTIVE' };
}
