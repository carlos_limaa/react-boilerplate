import update from 'react-addons-update';

export default function reducer(state = {
    error: false,
    notifications: [],
}, action) {
    switch (action.type) {
        case 'HOME_ADD_ERROR': {
            return { ...state, error: action.payload };
        }

        case 'HOME_REMOVE_ERROR': {
            return { ...state, error: false };
        }

        case 'HOME_ADD_NOTIFICATION': {
            return update(state, { notifications: { $push: [action.payload] } });
        }

        case 'HOME_REMOVE_NOTIFICATIONS': {
            return { ...state, notifications: [] };
        }
    }

    return state;
}
