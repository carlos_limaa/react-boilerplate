export function addError(payload) {
    return { type: 'HOME_ADD_ERROR', payload };
}

export function removeError() {
    return { type: 'HOME_REMOVE_ERROR' };
}

export function addNotification(payload) {
    return { type: 'HOME_ADD_NOTIFICATION', payload };
}

export function removeNotification() {
    return { type: 'HOME_REMOVE_NOTIFICATIONS' };
}

export function loading(value) {
    if (value) {
        return { type: 'HELPER_LOADING_ACTIVE' };
    }
    return { type: 'HELPER_LOADING_INACTIVE' };
}
