export function active() {
    return { type: 'HELPER_LOADING_ACTIVE' };
}

export function inactive() {
    return { type: 'HELPER_LOADING_INACTIVE' };
}
