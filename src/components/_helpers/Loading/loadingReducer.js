export default function reducer(state = {
    active: false,
}, action) {
    switch (action.type) {
        case 'HELPER_LOADING_ACTIVE': {
            return { ...state, active: true };
        }

        case 'HELPER_LOADING_INACTIVE': {
            return { ...state, active: false };
        }
    }
    return state;
}
