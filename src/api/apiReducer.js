export default function reducer(state = {
    error: false,
    activeNetwork: true,
    requesting: false,
}, action) {
    switch (action.type) {
        case 'API_ADD_ERROR_TIMEOUT': {
            return { ...state, error: action.payload };
        }

        case 'API_REMOVE_ERROR_TIMEOUT': {
            return { ...state, error: false };
        }

        case 'API_NETWORK_ACTIVE': {
            return { ...state, activeNetwork: true };
        }

        case 'API_NETWORK_INACTIVE': {
            return { ...state, activeNetwork: false };
        }

        case 'API_REQUESTING_ACTIVE': {
            return { ...state, requesting: true };
        }

        case 'API_REQUESTING_INACTIVE': {
            return { ...state, requesting: false };
        }
    }

    return state;
}
